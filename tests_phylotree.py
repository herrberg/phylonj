import unittest
import phylotree
import pandas

class base_test_nj_algo(unittest.TestCase):
    '''The case when distance matrix has a dimensionality of zero.'''

    filename = ''
    result = ';'

    def setUp(self):
        if self.filename:
            self.tree = phylotree.PhyloTree(self.filename)
        else:
            self.tree = phylotree.PhyloTree(pandas.DataFrame())

    def test_newick(self):
        if self.filename:
            self.assertEqual(self.tree.tree, self.result)

class test_nj_case1(base_test_nj_algo):
    filename = 'dm1.csv'
    result = '(((((a:2.0,b:3.0):3.0,c:4.0):2.0,d:2.0),e):1.0);'

class test_nj_case2(base_test_nj_algo):
    filename = 'dm2.csv'
    result = 'Node;'

class test_nj_case3(base_test_nj_algo):
    filename = 'dm3.csv'
    result = '((((((A:1.0,B:1.0):1.0,C:2.0):1.0,F:5.0):1.0,D:2.0),E):2.0);'

class test_nj_case4(base_test_nj_algo):
    filename = 'dm4.csv'
    result = '((FirstNode,SecondNode):10.0);'

if __name__ == '__main__':
    unittest.main()
        