import pandas as pd
import numpy as np
import ete3


class PhyloTree(object):
    '''PhyloTree runs a NJ algorithm to build a phylogenetic tree given a 
    distance matrix. The algorithm is ran when a class instance is initialized.
    The tree can be then access via .tree field in a newick-format
    representation.
    '''
    def __init__(self, distance_matrix):
        '''Instantiate a PhyloTree_NJ object.
        Params:
            distance_matrix -- Either a string containining a filename where
                               a distance matrix is stored, or a Pandas
                               DataFrame.
        '''
        if type(distance_matrix) is str:
            distance_matrix = pd.read_csv(distance_matrix, index_col=0)
        distance_matrix = distance_matrix.astype(float)
        while distance_matrix.shape[0] > 2:
            q = PhyloTree.calculate_q(distance_matrix)
            np.fill_diagonal(q.values, np.inf)
            d = q.idxmin().to_dict()
            a = min(q, key=lambda k: q[d[k]][k])
            b = d[a]
            d_a, d_b = PhyloTree.get_paired_dists(distance_matrix, a, b)
            u = '({}:{},{}:{})'.format(a, d_a, b, d_b)
            for c in distance_matrix.columns:
                if c == a or c == b:
                    continue
                dist = PhyloTree.get_dist_to_new_node(distance_matrix, a, b, c)
                distance_matrix[a][c] = dist
                distance_matrix[c][a] = dist
            t = {a: u}
            distance_matrix = distance_matrix.drop(b).drop(b, axis=1)
            distance_matrix = distance_matrix.rename(t).rename(t, axis=1)
        if distance_matrix.shape[0] < 2:
            if distance_matrix.shape[0] == 1:
                self.tree = '{};'.format(distance_matrix.columns[0])
            else:
                self.tree = ';'
        else:
            a, b = distance_matrix.columns
            self.tree = '(({},{}):{});'.format(a, b, distance_matrix[a][b])
        self.__tree = ete3.Tree(self.tree)

    def to_str(self):
        '''Gets a tree visualization in a string form.
        Returns:
            An ASCII-graphics-like visualization of a tree (str).
        '''
        return str(self.__tree)

    def to_image(self, filename):
        '''Saves a tree image to file.
        Params:
            filename -- An image filename.
        '''
        return self.__tree.render(filename)        

    @staticmethod
    def get_paired_dists(dm, a: str, b: str):
        '''Returns a distance from paired nodes a and b to new node u.
        Params:
            dm -- A distance matrix in a DataFrame form.
            a  -- A name of node a.
            b  -- A name of node b.
        Returns:
            A tuple (distance from a to u, distance from b to u).
        '''
        n = dm.shape[0]
        d_au = (dm[a][b] + (dm[a].sum() - dm[b].sum()) / (n - 2)) / 2
        d_bu = dm[a][b] - d_au
        return d_au, d_bu

    @staticmethod
    def get_dist_to_new_node(dm, a: str, b: str, c: str):
        '''Returns a distance from node c to a new node u formed by paired
        nodes a and b.
        Params:
            dm -- A distance matrix in a DataFrame form.
            a  -- A name of node a.
            b  -- A name of node b.
            c  -- A name of node c.
        Returns:
            A distance between c and u.
        '''
        return (dm[a][c] + dm[b][c] - dm[a][b]) / 2

    @staticmethod
    def calculate_q(distance_matrix):
        '''Calculate a Q matrix given a distance matrix.
        Params:
            distance_matrix -- A Pandas DataFrame.
        Returns:
            Q matrix as a Pandas DataFrame.
        '''
        d_s = distance_matrix.sum()
        q = (distance_matrix.shape[0] - 2) * distance_matrix
        return q.subtract(d_s, axis=0).subtract(d_s, axis=1)
        
    def __str__(self):
        return self.to_str()        